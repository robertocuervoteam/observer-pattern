from sources.observer import Observer
from sources.subject import Subject


class TestObserver(object):
    def test_observer(self):
        subject: Subject = Subject()
        observer1: Observer = Observer(subject, "Observer1")
        observer2: Observer = Observer(subject, "Observer2")
        observer3: Observer = Observer(subject, "Observer3")
        message: str = "message 1"
        subject.send_message(message)
        assert message == observer1.get_message()
        assert message == observer2.get_message()
        assert message == observer3.get_message()
        message2: str = "message 2"
        subject.send_message(message2)
        assert message2 == observer1.get_message()
        assert message2 == observer2.get_message()
        assert message2 == observer3.get_message()
