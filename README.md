# Observer pattern in python
Implementation of the behavioural Observer pattern as exercise
## Install dependencies

Run `python3 -m pip install -r requirements.txt` under the `observer-pattern` directory.

## Run the example
Just `cd test` and run:
` python3 -m pytest -s`