from typing import ClassVar, List
from .subject_interface import ISubject
from .observer_interface import IObserver


class Subject(ISubject):
    def __init__(self) -> None:
        self.observers: ClassVar[List] = []
        self.message: ClassVar[str] = ''

    def notify(self) -> None:
        for observer in self.observers:
            observer.update(self.message)

    def register(self, observer: IObserver) -> None:
        self.observers.append(observer)
        print(f"Added new observer {observer}")

    def unregister(self, observer: IObserver) -> None:
        self.observers.remove(observer)
        print(f"Removed observer {observer}")

    def send_message(self, message: str) -> None:
        self.message = message
        self.notify()
