import logging
from typing import ClassVar, Optional
from .observer_interface import IObserver
from .subject_interface import ISubject

logger = logging.getLogger(__name__)


class Observer(IObserver):

    def __init__(self, subject: ISubject, observer_name: str) -> None:
        self.subject: ClassVar[ISubject] = subject
        self.name: ClassVar[str] = observer_name
        self.message: ClassVar[str] = ''
        self.subject.register(self)

    def update(self, message: str) -> None:
        self.message = message
        print(f"{self.name} has new message available: {message}")

    def get_message(self) -> str:
        return self.message

    def __repr__(self) -> str:
        return f"{self.name} with message: {self.get_message()}"
