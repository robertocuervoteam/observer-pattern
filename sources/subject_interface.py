import abc
from .observer_interface import IObserver


class ISubject(metaclass=abc.ABCMeta):
    @classmethod
    def __subclasshook__(cls, subclass):
        return (hasattr(subclass, 'update') and
                callable(subclass.update) and
                hasattr(subclass, 'register') and
                callable(subclass.register) and
                hasattr(subclass, 'unregister') and
                callable(subclass.unregister) or
                NotImplemented)

    @abc.abstractmethod
    def notify(self) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def register(self, observer: IObserver) -> None:
        raise NotImplementedError

    @abc.abstractmethod
    def unregister(self, observer: IObserver) -> None:
        raise NotImplementedError
